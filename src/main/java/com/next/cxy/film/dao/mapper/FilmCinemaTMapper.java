package com.next.cxy.film.dao.mapper;

import com.next.cxy.film.controller.cinema.vo.CinemaFilmVO;
import com.next.cxy.film.dao.entity.FilmCinemaT;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 影院信息表 Mapper 接口
 * </p>
 *
 * @author cxy
 * @since 2020-07-10
 */
public interface FilmCinemaTMapper extends BaseMapper<FilmCinemaT> {

}
