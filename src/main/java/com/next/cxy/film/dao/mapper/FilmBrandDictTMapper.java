package com.next.cxy.film.dao.mapper;

import com.next.cxy.film.dao.entity.FilmBrandDictT;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 品牌信息表 Mapper 接口
 * </p>
 *
 * @author cxy
 * @since 2020-07-10
 */
public interface FilmBrandDictTMapper extends BaseMapper<FilmBrandDictT> {

}
