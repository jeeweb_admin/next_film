package com.next.cxy.film.dao.mapper;

import com.next.cxy.film.dao.entity.FilmBannerT;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * banner信息表 Mapper 接口
 * </p>
 *
 * @author cxy
 * @since 2020-07-28
 */
public interface FilmBannerTMapper extends BaseMapper<FilmBannerT> {

}
