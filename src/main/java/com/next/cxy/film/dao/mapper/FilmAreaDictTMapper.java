package com.next.cxy.film.dao.mapper;

import com.next.cxy.film.dao.entity.FilmAreaDictT;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 地域信息表 Mapper 接口
 * </p>
 *
 * @author cxy
 * @since 2020-07-10
 */
public interface FilmAreaDictTMapper extends BaseMapper<FilmAreaDictT> {

}
