package com.next.cxy.film.dao.mapper;

import com.next.cxy.film.dao.entity.FilmHallFilmInfoT;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 影厅电影信息表 Mapper 接口
 * </p>
 *
 * @author cxy
 * @since 2020-07-10
 */
public interface FilmHallFilmInfoTMapper extends BaseMapper<FilmHallFilmInfoT> {

}
