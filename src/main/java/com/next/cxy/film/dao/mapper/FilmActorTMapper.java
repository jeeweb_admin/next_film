package com.next.cxy.film.dao.mapper;

import com.next.cxy.film.controller.film.vo.response.filmdetail.ActorResultVO;
import com.next.cxy.film.dao.entity.FilmActorT;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 演员表 Mapper 接口
 * </p>
 *
 * @author cxy
 * @since 2020-07-28
 */
public interface FilmActorTMapper extends BaseMapper<FilmActorT> {
    /**
     * 根据电影编号获取演员信息
     * @param filmId
     * @return
     */
    List<ActorResultVO> describeActorsByFilmId(@Param("filmId")String filmId);
}
