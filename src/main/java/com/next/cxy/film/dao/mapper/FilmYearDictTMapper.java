package com.next.cxy.film.dao.mapper;

import com.next.cxy.film.dao.entity.FilmYearDictT;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 年代信息表 Mapper 接口
 * </p>
 *
 * @author cxy
 * @since 2020-07-28
 */
public interface FilmYearDictTMapper extends BaseMapper<FilmYearDictT> {

}
