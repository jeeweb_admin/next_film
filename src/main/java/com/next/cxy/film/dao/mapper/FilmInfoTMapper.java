package com.next.cxy.film.dao.mapper;

import com.next.cxy.film.controller.film.vo.response.filmdetail.FilmDetailResultVO;
import com.next.cxy.film.dao.entity.FilmInfoT;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 影片主表 Mapper 接口
 * </p>
 *
 * @author cxy
 * @since 2020-07-28
 */
public interface FilmInfoTMapper extends BaseMapper<FilmInfoT> {
    /**
     * 根据FilmId获取电影的详情
     * @param filmName
     * @return
     */
    FilmDetailResultVO descibeFilmDetailByFilmId(@Param("filmId")String filmName);

    /**
     * 根据电影名称获取电影的详情-模糊匹配
     * @param filmName
     * @return
     */
    FilmDetailResultVO describeFilmDetailByFilmName(@Param("filmName")String filmName);
}
