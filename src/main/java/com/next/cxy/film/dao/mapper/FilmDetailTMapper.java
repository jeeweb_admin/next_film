package com.next.cxy.film.dao.mapper;

import com.next.cxy.film.dao.entity.FilmDetailT;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 影片主表 Mapper 接口
 * </p>
 *
 * @author cxy
 * @since 2020-07-28
 */
public interface FilmDetailTMapper extends BaseMapper<FilmDetailT> {

}
