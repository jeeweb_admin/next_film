package com.next.cxy.film.dao.mapper;

import com.next.cxy.film.dao.entity.NextUserT;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author cxy
 * @since 2020-07-08
 */
public interface NextUserTMapper extends BaseMapper<NextUserT> {

}
