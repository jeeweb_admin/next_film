package com.next.cxy.film.dao.mapper;

import com.next.cxy.film.controller.cinema.vo.CinemaFilmInfoVO;
import com.next.cxy.film.controller.cinema.vo.CinemaFilmVO;
import com.next.cxy.film.controller.cinema.vo.FieldHallInfoVO;
import com.next.cxy.film.dao.entity.FilmFieldT;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 放映场次表 Mapper 接口
 * </p>
 *
 * @author cxy
 * @since 2020-07-10
 */
public interface FilmFieldTMapper extends BaseMapper<FilmFieldT> {
    /**
     * 根据电影院ID查询放映场次
     * @param cinemaId
     * @return
     */
    List<CinemaFilmVO> describeFieldList(@Param("cinemaId")String cinemaId);

    /**
     * 根据场次ID查询电影信息
     * @param fieldId
     * @return
     */
    CinemaFilmInfoVO describeFilmInfoByFieldId(@Param("fieldId")String fieldId);

    /**
     * 根据场次ID查询作为信息
     * @param fieldId
     * @return
     */
    FieldHallInfoVO describeHallInfo(@Param("fieldId")String fieldId);
}
