package com.next.cxy.film.dao.mapper;

import com.next.cxy.film.dao.entity.FilmSourceDictT;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 区域信息表 Mapper 接口
 * </p>
 *
 * @author cxy
 * @since 2020-07-28
 */
public interface FilmSourceDictTMapper extends BaseMapper<FilmSourceDictT> {

}
