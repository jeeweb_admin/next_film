package com.next.cxy.film.service.user;

import com.next.cxy.film.controller.vo.EnrollUserVO;
import com.next.cxy.film.controller.vo.UserInfoVO;
import com.next.cxy.film.dao.entity.NextUserT;
import com.baomidou.mybatisplus.extension.service.IService;
import com.next.cxy.film.service.common.exception.CommonServiceExcetion;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author cxy
 * @since 2020-07-08
 */
public interface INextUserTService extends IService<NextUserT> {
    /**
     * 用户登记接口
     *
     * @param nextUserTVO
     * @throws CommonServiceExcetion
     */

    void userEnroll(EnrollUserVO nextUserTVO) throws CommonServiceExcetion;

    /**
     * 验证用户名是否存在
     *
     * @param userName
     * @return
     * @throws CommonServiceExcetion
     */
    boolean checkUserName(String userName) throws CommonServiceExcetion;

    /**
     * 用户登录接口
     *
     * @param userName
     * @param userPwd
     * @return
     * @throws CommonServiceExcetion
     */
    boolean userAuth(String userName, String userPwd) throws CommonServiceExcetion;

    /**
     * 获取用户信息
      * @param userId
     * @return
     */
    UserInfoVO describeUserInfo(String userId)throws CommonServiceExcetion;

    /**
     * 修改用户信息
     * @param userInfoVO
     * @return
     */
    UserInfoVO updateUserInfo(UserInfoVO userInfoVO)throws CommonServiceExcetion;
}
