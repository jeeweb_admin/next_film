package com.next.cxy.film.service.film;

import com.next.cxy.film.controller.film.vo.response.condition.CatInfoResultVO;
import com.next.cxy.film.controller.film.vo.response.condition.SourceInfoResultVO;
import com.next.cxy.film.controller.film.vo.response.condition.YearInfoResultVO;
import com.next.cxy.film.controller.film.vo.response.index.BannerInfoResultVO;
import com.next.cxy.film.controller.film.vo.response.index.HotListResultVO;
import com.next.cxy.film.controller.film.vo.response.index.RankFilmListResultVO;
import com.next.cxy.film.controller.film.vo.response.index.SoonListResultVO;
import com.next.cxy.film.service.common.exception.CommonServiceExcetion;


import java.util.List;

public interface FilmServiceApi {

    /**
     * Banner信息获取
     */

    List<BannerInfoResultVO> describeBanners() throws CommonServiceExcetion;

    //拆分-合并
    /**
     * 获取热门影片
     * @return
     */
    List<HotListResultVO>describeHostFilms() throws CommonServiceExcetion;

    /**
     * 获取即将上映影片
     * @return
     */
    List<SoonListResultVO>describeSoonFilms() throws CommonServiceExcetion;

    /**
     * 票房排行
     * @return
     */
    List<RankFilmListResultVO>boxRandFilms() throws CommonServiceExcetion;

    /**
     * 期待排行
     * @return
     */
    List<RankFilmListResultVO>expectRandFilms() throws CommonServiceExcetion;


    /**
     * top100排行
     * @return
     */
    List<RankFilmListResultVO>topRandFilms() throws CommonServiceExcetion;


    /**
     * 验证有效性
     *
     *
     */
    String checkCondition(String conditionId,String type)throws CommonServiceExcetion;

    /**
     * 三种条件查询
     */
    List<CatInfoResultVO>describeCatInfo(String catId);
    List<SourceInfoResultVO>describeSourceInfo(String sourceId);
    List<YearInfoResultVO>describeYearInfo(String yearId);

}
