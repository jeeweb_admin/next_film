package com.next.cxy.film.service.cinema.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Lists;
import com.next.cxy.film.controller.cinema.vo.*;
import com.next.cxy.film.controller.condition.AreaResVO;
import com.next.cxy.film.controller.condition.BrandResVO;
import com.next.cxy.film.controller.condition.HallTypeResVO;
import com.next.cxy.film.controller.cinema.vo.request.DescribeCinemaRequestVO;
import com.next.cxy.film.dao.entity.FilmAreaDictT;
import com.next.cxy.film.dao.entity.FilmBrandDictT;
import com.next.cxy.film.dao.entity.FilmCinemaT;
import com.next.cxy.film.dao.entity.FilmHallDictT;
import com.next.cxy.film.dao.mapper.*;
import com.next.cxy.film.service.cinema.ICinemaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CinemaServiceImpl implements ICinemaService {
    @Autowired
    private FilmFieldTMapper filmFieldTMapper;
    @Autowired
    private FilmCinemaTMapper filmCinemaTMapper;
    @Autowired
    private FilmHallFilmInfoTMapper filmHallFilmInfoTMapper;
    @Autowired
    private FilmAreaDictTMapper filmAreaDictTMapper;
    @Autowired
    private FilmHallDictTMapper filmHallDictTMapper;
    @Autowired
    private FilmBrandDictTMapper filmBrandDictTMapper;

    @Override
    public Page<CinemaVO> describeCinemaInfo(DescribeCinemaRequestVO describeCinemaRequestVO) {
        //组织分页对象
        Page<FilmCinemaT> page = new Page<>(
                describeCinemaRequestVO.getNowPage(),
                describeCinemaRequestVO.getPageSize());
        //组织查询对象
        QueryWrapper<FilmCinemaT> queryWrapper = describeCinemaRequestVO.genWrapper();
        //获取数据库返回
        IPage<FilmCinemaT> filmCinemaTIPage = filmCinemaTMapper.selectPage(page, queryWrapper);
        //组织返回值
        Page<CinemaVO> cinemaPage = new Page<>(describeCinemaRequestVO.getNowPage(), describeCinemaRequestVO.getPageSize());

        cinemaPage.setTotal(page.getTotal());

        //将数据实体转换为表现层展示对象
        List<CinemaVO> cinemas = filmCinemaTIPage.getRecords().stream().map((data) -> {
            //数据转换
            CinemaVO build = CinemaVO.builder().
                    uuid(data.getUuid() + "")
                    .cinemaName(data.getCinemaName())
                    .address(data.getCinemaAddress())
                    .minimumPrice(data.getMinimumPrice() + "")
                    .build();

            return build;
        }).collect(Collectors.toList());

        cinemaPage.setRecords(cinemas);

        return cinemaPage;
    }

    @Override
    public boolean checkCondition(int conditionId, String conditionType) {
        //验证brandId是否有效
        //如果无效，则应该将brrandId=99,并且将brandId=99的isActive设置为true
        switch (conditionType) {
            case "brand":
                FilmBrandDictT filmBrandDictT = filmBrandDictTMapper.selectById(conditionId);
                if (filmBrandDictT != null && filmBrandDictT.getUuid() != null) {
                    return true;
                } else {
                    return false;
                }
            case "area":
                FilmAreaDictT filmAreaDictT = filmAreaDictTMapper.selectById(conditionId);
                if (filmAreaDictT != null && filmAreaDictT.getUuid() != null) {
                    return true;
                } else {
                    return false;
                }
            case "hallType":
                FilmHallDictT filmHallDictT = filmHallDictTMapper.selectById(conditionId);
                if (filmHallDictT != null && filmHallDictT.getUuid() != null) {
                    return true;
                } else {
                    return false;
                }
        }
        return false;
    }

    @Override
    public List<BrandResVO> describeBrandConditions(int brandId) {

        //如果有效将能匹配的brand对应的isActtive设值为true
        List<FilmBrandDictT> filmBrandDictTS = filmBrandDictTMapper.selectList(null);
        //并且将对应的品牌设置为isActive=true
        List<BrandResVO> collect = filmBrandDictTS.stream().map((data -> {
            BrandResVO brandResVO = new BrandResVO();
            if (brandId == data.getUuid()) {
                brandResVO.setIsActive("true");
            } else {
                brandResVO.setIsActive("false");
            }
            brandResVO.setBrandId(data.getUuid() + "");
            brandResVO.setBrandName(data.getShowName());
            return brandResVO;
        })).collect(Collectors.toList());


        return collect;
    }

    @Override
    public List<AreaResVO> describeAreaConditions(int areaId) {
        //获取所有列表
        List<FilmAreaDictT>areaDictTS=filmAreaDictTMapper.selectList(null);
        List<AreaResVO> collect = areaDictTS.stream().map((data) -> {
            AreaResVO areaResVO = new AreaResVO();
            if (areaId == data.getUuid()) {
                areaResVO.setIsActive("true");
            } else {
                areaResVO.setIsActive("false");
            }
            areaResVO.setAreaId(data.getUuid() + "");
            areaResVO.setAreaName(data.getShowName());
            return areaResVO;
        }).collect(Collectors.toList());
        return collect;
    }

    @Override
    public List<HallTypeResVO> describeHallTypeConditions(int halltypeId) {

        //获取所有列表
        List<FilmHallDictT> hallTypeDicts = filmHallDictTMapper.selectList(null);
        //并且将对应的区域设值味isActive=true
        List<HallTypeResVO> collect = hallTypeDicts.stream().map((data) -> {
            HallTypeResVO hallTypeResVO = new HallTypeResVO();
            if (halltypeId == data.getUuid()) {
                hallTypeResVO.setIsActive("true");
            } else {
                hallTypeResVO.setIsActive("false");
            }
            hallTypeResVO.setHalltypeId(data.getUuid() + "");
            hallTypeResVO.setHalltypeName(data.getShowName());
            return hallTypeResVO;
        }).collect(Collectors.toList());
        return collect;
    }

    @Override
    public CinemaDetailVO describeCinemaDetails(String cinemaId) {
        FilmCinemaT data = filmCinemaTMapper.selectById(cinemaId);
        CinemaDetailVO cinemaDetailVO = CinemaDetailVO.builder()
                .cinemaAdress(data.getCinemaAddress())
                .cinemaId(data.getUuid() + "")
                .cinemaName(data.getCinemaName())
                .cinemaPhone(data.getCinemaPhone())
                .imgUrl(data.getImgAddress()).build();
        return cinemaDetailVO;
    }

    @Override
    public List<CinemaFilmVO> describeFieldsAndFilmInfo(String cinemaId) {
        //确认CinemaId是否有效
        QueryWrapper objectQueryWrapper = new QueryWrapper<>();
        objectQueryWrapper.eq("uuid", cinemaId);
        Integer isNull = filmCinemaTMapper.selectCount(objectQueryWrapper);
        if (isNull == 0) {
            return Lists.newArrayList();
        }
        return filmFieldTMapper.describeFieldList(cinemaId);
    }

    @Override
    public CinemaFilmInfoVO describeFilmInfoByFieldId(String fieldId) {
        return filmFieldTMapper.describeFilmInfoByFieldId(fieldId);
    }

    @Override
    public FieldHallInfoVO describeHallInfoByFieldId(String fieldId) {
        return filmFieldTMapper.describeHallInfo(fieldId);
    }
}
