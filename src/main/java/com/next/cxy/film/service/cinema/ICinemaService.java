package com.next.cxy.film.service.cinema;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.next.cxy.film.controller.cinema.vo.*;
import com.next.cxy.film.controller.condition.AreaResVO;
import com.next.cxy.film.controller.condition.BrandResVO;
import com.next.cxy.film.controller.condition.HallTypeResVO;
import com.next.cxy.film.controller.cinema.vo.request.DescribeCinemaRequestVO;

import java.util.List;

public interface ICinemaService {
    /**
     * 根据条件查询用户列表
     * @param describeCinemaRequestVO
     * @return
     */
    Page<CinemaVO> describeCinemaInfo(DescribeCinemaRequestVO describeCinemaRequestVO);

    /**
     * 获取查询条件
     * @param brandId
     * @return
     */
    boolean checkCondition(int conditionId, String conditionType);
    List<BrandResVO>describeBrandConditions(int brandId);
    List<AreaResVO>describeAreaConditions(int areaId);
    List<HallTypeResVO>describeHallTypeConditions(int halltypeId);

    /**
     * 根据影院编号获取影院信息
     */
    CinemaDetailVO describeCinemaDetails(String cinemaId);
    /**
     * 根据影院编号获取场次信息
     */
    List<CinemaFilmVO>describeFieldsAndFilmInfo(String cinemaId);
    /**
     * 根据场次编号，获取电影信息
     */
    CinemaFilmInfoVO describeFilmInfoByFieldId(String fieldId);
    /**
     * 根据场次编号，获取放映厅幸喜
     */
    FieldHallInfoVO describeHallInfoByFieldId(String fieldId);
}
