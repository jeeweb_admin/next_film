package com.next.cxy.film.service.common.exception;

public class DataTranctionExcetion extends Exception {
    private Integer code;
    private String errMsg;

    public DataTranctionExcetion(Integer code, String errMsg) {
        super(errMsg);
        this.code = code;
        this.errMsg = errMsg;
    }
}
