package com.next.cxy.film.controller.auth.controller.vo;


import com.next.cxy.film.controller.common.BaseResponseVO;
import com.next.cxy.film.controller.common.BaseVO;
import com.next.cxy.film.controller.exception.ParamErrorExcetion;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

@Data
@Builder
public class AuthRequestVO extends BaseVO {
    private String username;
    private String password;

    @Tolerate
    AuthRequestVO() {}

    @Override
    public void checkParam() throws ParamErrorExcetion {
        //TODO 验证过程
    }
}
