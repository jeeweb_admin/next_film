package com.next.cxy.film.controller.vo;

import com.next.cxy.film.controller.common.BaseVO;
import com.next.cxy.film.controller.exception.ParamErrorExcetion;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

@Data
@Builder
public class UserInfoVO extends BaseVO {
    private Integer id;
    private Integer uuid;
    private String username;
    private String nickname;
    private String email;
    private String phone;
    private Integer sex;
    private String birthday;
    private String lifeState;
    private String biography;
    private String headAddress;
    private Long beginTime;
    private Long updateTime;

    @Tolerate
    UserInfoVO() {}

    public Integer getUuid() {
        return this.getId();
    }

    @Override
    public void checkParam() throws ParamErrorExcetion {
        //加入自己的验证逻辑
    }
}
