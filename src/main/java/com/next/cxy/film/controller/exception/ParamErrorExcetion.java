package com.next.cxy.film.controller.exception;

import lombok.Data;

@Data
public class ParamErrorExcetion extends Exception {
    private Integer code;
    private String errMsg;

    public ParamErrorExcetion(int code,String errMsg){
        super(errMsg);
        this.code=code;
        this.errMsg=errMsg;
    }
}
