package com.next.cxy.film.controller.auth.controller.vo;

import com.next.cxy.film.controller.common.BaseVO;
import com.next.cxy.film.controller.exception.ParamErrorExcetion;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AuthResponseVO  extends BaseVO {
    private String randomKey;
    private String token;

    @Override
    public void checkParam() throws ParamErrorExcetion {

    }
}
