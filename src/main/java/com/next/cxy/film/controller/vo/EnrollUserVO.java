package com.next.cxy.film.controller.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.next.cxy.film.common.utils.ToolUtils;
import com.next.cxy.film.controller.common.BaseVO;
import com.next.cxy.film.controller.exception.ParamErrorExcetion;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Tolerate;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 用户表
 * </p>
 * 使用Builder注解会生成带参的构造函数，导致不会提供无参构造函数，需要手动提供一个
 * @author cxy
 * @since 2020-07-08
 */
@Data
@Builder
public class EnrollUserVO extends BaseVO {

    private String userName;
    private String password;
    private String email;
    private String phone;
    private String address;
    @Tolerate
    EnrollUserVO() {}
    @Override
    public void checkParam() throws ParamErrorExcetion {
        if(ToolUtils.isEmpty(userName)){
            throw new ParamErrorExcetion(400,"用户名不能为空！");
        }
        if(ToolUtils.isEmpty(password)){
            throw new ParamErrorExcetion(400,"密码不能为空！");
        }
        //用户名不能超过20位.......

    }
}
