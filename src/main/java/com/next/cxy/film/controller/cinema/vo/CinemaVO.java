package com.next.cxy.film.controller.cinema.vo;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * 影院列表实体
 */
@Data
@Builder
public class CinemaVO implements Serializable {

    private String uuid;
    private String cinemaName;
    private String address;
    private String minimumPrice;
}
