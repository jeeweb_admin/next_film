package com.next.cxy.film.controller.cinema.vo.response;

import com.next.cxy.film.controller.common.BaseVO;
import com.next.cxy.film.controller.exception.ParamErrorExcetion;
import lombok.Data;

import java.io.Serializable;

@Data
public class FieldInfoRequestVO  extends BaseVO implements Serializable {

    private String cinemaId;
    private String fieldId;

    @Override
    public void checkParam() throws ParamErrorExcetion {

    }
}
