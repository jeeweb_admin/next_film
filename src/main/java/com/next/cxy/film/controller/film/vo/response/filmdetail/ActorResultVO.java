package com.next.cxy.film.controller.film.vo.response.filmdetail;

import lombok.Data;

import java.io.Serializable;

/**
 * 演员实体
 */
@Data
public class ActorResultVO implements Serializable {

    private String imgAddress;
    private String directorName;
    private String roleName;
}
