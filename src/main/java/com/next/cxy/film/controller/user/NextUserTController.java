package com.next.cxy.film.controller.user;


import com.next.cxy.film.common.utils.ToolUtils;
import com.next.cxy.film.controller.common.BaseResponseVO;
import com.next.cxy.film.controller.common.TraceUtil;
import com.next.cxy.film.controller.exception.NextFilmException;
import com.next.cxy.film.controller.exception.ParamErrorExcetion;
import com.next.cxy.film.controller.vo.EnrollUserVO;
import com.next.cxy.film.controller.vo.UserInfoVO;
import com.next.cxy.film.service.common.exception.CommonServiceExcetion;
import com.next.cxy.film.service.user.INextUserTService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author cxy
 * @since 2020-07-08
 */
@RestController
@RequestMapping("/nextUserT")
@Api("用户模块相关API")
public class NextUserTController {
    @Autowired
    private INextUserTService nextUserTService;

    @ApiOperation(value = "用户名重复性验证",notes = "用户名重复性验证")
    @ApiImplicitParam(name="username",value = "待验证的用户名称",paramType = "query",required = true,dataType = "string")
    @RequestMapping(value = "check", method = RequestMethod.POST)
    public BaseResponseVO checkUser(String username) throws CommonServiceExcetion, NextFilmException {
        if(ToolUtils.isEmpty(username)){
            throw  new NextFilmException(1,"username不能为null");
        }
        boolean isSucess = nextUserTService.checkUserName(username);
        if(isSucess){
            return BaseResponseVO.serviceFailed("用户名已存在！");
        }else {
            return BaseResponseVO.success();
        }
    }

    @RequestMapping(value = "register", method = RequestMethod.POST)
    public BaseResponseVO register(@RequestBody EnrollUserVO enrollUserVO) throws CommonServiceExcetion, ParamErrorExcetion {
            // 贫血模型
        enrollUserVO.checkParam();
        nextUserTService.userEnroll(enrollUserVO);
        return BaseResponseVO.success();
    }

    @RequestMapping(value = "getUserInfo", method = RequestMethod.GET)
    public BaseResponseVO describeUserInfo() throws CommonServiceExcetion, ParamErrorExcetion {
        String userId= TraceUtil.getUserId();
        UserInfoVO userInfoVO = nextUserTService.describeUserInfo(userId);
        userInfoVO.checkParam();
        return BaseResponseVO.success(userInfoVO);
    }

    @RequestMapping(value = "updateUserInfo", method = RequestMethod.POST)
    public BaseResponseVO updateUserInfo(@RequestBody UserInfoVO userInfoVO) throws CommonServiceExcetion, ParamErrorExcetion {
        userInfoVO.checkParam();
        String userId= TraceUtil.getUserId();
        UserInfoVO result = nextUserTService.updateUserInfo(userInfoVO);
        userInfoVO.checkParam();
        return BaseResponseVO.success(result);
    }

    @RequestMapping(value = "logout", method = RequestMethod.POST)
    public BaseResponseVO logout() throws CommonServiceExcetion, ParamErrorExcetion {
        String userId = TraceUtil.getUserId();
        /**
         * 正常用户退出
         * 1.用户信息放入Redis缓存
         * 2.去掉用户缓存
         */
        return BaseResponseVO.success();
    }
}
