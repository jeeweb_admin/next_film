package com.next.cxy.film.controller.cinema.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.next.cxy.film.config.properties.FilmProperties;
import com.next.cxy.film.controller.cinema.vo.*;
import com.next.cxy.film.controller.cinema.vo.request.DescribeCinemaRequestVO;
import com.next.cxy.film.controller.cinema.vo.response.FieldInfoRequestVO;
import com.next.cxy.film.controller.common.BaseResponseVO;
import com.next.cxy.film.controller.condition.AreaResVO;
import com.next.cxy.film.controller.condition.BrandResVO;
import com.next.cxy.film.controller.condition.HallTypeResVO;
import com.next.cxy.film.service.cinema.ICinemaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/cinema")
public class CinemaController {
    @Autowired
    private FilmProperties filmProperties;

    @Autowired
    private ICinemaService cinemaService;

    @RequestMapping(value = "getFields", method = RequestMethod.GET)
    public BaseResponseVO getFields(String cinemaId) {
        //获取元数据
        List<CinemaFilmVO> cinemaFilms = cinemaService.describeFieldsAndFilmInfo(cinemaId);
        CinemaDetailVO cinemaDetailVO = cinemaService.describeCinemaDetails(cinemaId);
        //组织返回参数
        List<Map<String, CinemaFilmVO>> arrayList = Lists.newArrayList();

        cinemaFilms.stream().forEach((film) -> {
            Map<String, CinemaFilmVO> filmVOMap = new HashMap<>();
            filmVOMap.put("filmInfo", film);
            arrayList.add(filmVOMap);
        });

        Map<String, Object> resultMap = Maps.newHashMap();
        resultMap.put("cinemaInfo", cinemaDetailVO);
        resultMap.put("filmList", arrayList);
        return BaseResponseVO.success(filmProperties.getImgPre(), resultMap);


    }

    @RequestMapping(value = "/getFieldInfo", method = RequestMethod.POST)
    public BaseResponseVO getFieldInfo(@RequestBody FieldInfoRequestVO requestVO) {
        //获取逻辑层调用结果
        CinemaDetailVO cinemaDetailVO = cinemaService.describeCinemaDetails(requestVO.getCinemaId());
        FieldHallInfoVO fieldHallInfoVO = cinemaService.describeHallInfoByFieldId(requestVO.getFieldId());
        CinemaFilmInfoVO cinemaFilmInfoVO = cinemaService.describeFilmInfoByFieldId(requestVO.getFieldId());
        //组织返回参数
        Map<String, Object> result = Maps.newHashMap();
        result.put("filmInfo", fieldHallInfoVO);
        result.put("cinemaInfo", cinemaDetailVO);
        result.put("hallInfo", cinemaFilmInfoVO);
        return BaseResponseVO.success(filmProperties.getImgPre(), result);
    }

    @RequestMapping(value = "/getCinemas", method = RequestMethod.GET)
    public BaseResponseVO getCinemas(DescribeCinemaRequestVO requestVO) {
        Page<CinemaVO> cinemaVOPage = cinemaService.describeCinemaInfo(requestVO);

        //组织返回参数
        Map<String, Object> result = Maps.newHashMap();
        result.put("filmInfo", cinemaVOPage.getRecords());
        return BaseResponseVO.success(cinemaVOPage.getCurrent(), cinemaVOPage.getPages(), filmProperties.getImgPre(), result);
    }

    @RequestMapping(value = "/getCondition",method = RequestMethod.GET)
    public BaseResponseVO getCondition(
            @RequestParam(name="brandId",required = false,defaultValue = "99")Integer brandId,
            @RequestParam(name="hallType",required = false,defaultValue = "99")Integer hallType,
            @RequestParam(name="areaId",required = false,defaultValue = "99")Integer areaId
    ){
        //验证id是否有效
        if(!cinemaService.checkCondition(brandId,"brand")){
            brandId=99;
        }
        if(!cinemaService.checkCondition(brandId,"area")){
            hallType=99;
        }
        if(!cinemaService.checkCondition(brandId,"hallType")){
            areaId=99;
        }

        //获取逻辑层结果
        List<BrandResVO> brandResVOS = cinemaService.describeBrandConditions(brandId);
        List<AreaResVO> areaResVOS = cinemaService.describeAreaConditions(areaId);
        List<HallTypeResVO> hallTypeResVOS = cinemaService.describeHallTypeConditions(hallType);

        //组织返回参数
        Map<String, Object> result = Maps.newHashMap();
        result.put("areaList",areaResVOS);
        result.put("halltypeList",hallTypeResVOS);
        result.put("brandList",brandResVOS);
        return BaseResponseVO.success(filmProperties.getImgPre(), result);
    }

}
