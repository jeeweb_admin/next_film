package com.next.cxy.film.controller.common;

import com.next.cxy.film.controller.exception.ParamErrorExcetion;

public abstract class BaseVO  {
    public abstract void checkParam() throws ParamErrorExcetion;
}
