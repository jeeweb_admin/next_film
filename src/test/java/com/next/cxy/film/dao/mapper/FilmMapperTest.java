package com.next.cxy.film.dao.mapper;

import com.next.cxy.film.controller.film.vo.response.filmdetail.ActorResultVO;
import com.next.cxy.film.controller.film.vo.response.filmdetail.FilmDetailResultVO;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
class FilmMapperTest {
    @Autowired
    private FilmInfoTMapper filmInfoTMapper;
    @Autowired
    private FilmActorTMapper actorTMapper;

    @Test
    public void describeActorsByFilmId(){
        String filmId="100";
        List<ActorResultVO> actorResultVOS = actorTMapper.describeActorsByFilmId(filmId);
        System.out.println("actorResultVOS by Id:"+actorResultVOS);
        actorResultVOS.stream().forEach((actor)-> System.out.println(actor));
    }

    @Test
    public void descibeFilmDetailByFilmId(){
        String filmId="100";
        FilmDetailResultVO filmDetailResultVO = filmInfoTMapper.descibeFilmDetailByFilmId(filmId);
        System.out.println("filmDetailResultVO by Id:"+filmDetailResultVO);

    }

    @Test
    public void describeFilmDetailByFilmName(){
        String filmName="我们";
        FilmDetailResultVO filmDetailResultVO = filmInfoTMapper.describeFilmDetailByFilmName(filmName);
        System.out.println("filmDetailResultVO by name:"+filmDetailResultVO);
    }
}