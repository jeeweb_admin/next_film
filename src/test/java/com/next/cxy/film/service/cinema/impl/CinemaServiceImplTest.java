package com.next.cxy.film.service.cinema.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.next.cxy.film.controller.cinema.vo.CinemaVO;
import com.next.cxy.film.controller.condition.BrandResVO;
import com.next.cxy.film.controller.cinema.vo.request.DescribeCinemaRequestVO;
import com.next.cxy.film.service.cinema.ICinemaService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;


@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
class CinemaServiceImplTest {

    @Autowired
    private ICinemaService cinemaService;

    @Test
    void describeCinemaInfo() {
        DescribeCinemaRequestVO describeCinemaRequestVO = new DescribeCinemaRequestVO();
        describeCinemaRequestVO.setBrandId(1);
        describeCinemaRequestVO.setDistrictId(1);
        describeCinemaRequestVO.setHallType(1);
        Page<CinemaVO> page = cinemaService.describeCinemaInfo(describeCinemaRequestVO);
        log.info("nowPage:{},totalPage:{};recordNum:{},records:{}",page.getCurrent(),page.getPages(),page.getTotal(),page.getRecords());

    }

    @Test
    void describeBrandConditions() {
        List<BrandResVO> brandResVOS = cinemaService.describeBrandConditions(1);
        brandResVOS.stream().forEach(data->System.out.println(data));

    }

    @Test
    void describeAreaConditions() {
    }

    @Test
    void describeHallTypeConditions() {
    }

    @Test
    void describeCinemaDetails() {
        System.out.println(cinemaService.describeCinemaDetails("1"));
    }

    @Test
    void describeFieldsAndFilmInfo() {
        System.out.println(cinemaService.describeFieldsAndFilmInfo("1"));
    }

    @Test
    void describeFilmInfoByFieldId() {
        System.out.println(cinemaService.describeFilmInfoByFieldId("1"));
    }

    @Test
    void describeHallInfoByFieldId() {
        System.out.println(cinemaService.describeHallInfoByFieldId("1"));
    }
}