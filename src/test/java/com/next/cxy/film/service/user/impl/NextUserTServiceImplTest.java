package com.next.cxy.film.service.user.impl;
import com.next.cxy.film.controller.vo.EnrollUserVO;
import com.next.cxy.film.controller.vo.UserInfoVO;
import com.next.cxy.film.service.common.exception.CommonServiceExcetion;
import com.next.cxy.film.service.user.INextUserTService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
class NextUserTServiceImplTest {

    @Autowired
    private INextUserTService iNextUserTService;

    @Test
    void userEnroll() throws CommonServiceExcetion {
       EnrollUserVO enrollUserVO= EnrollUserVO.builder()
                .userName("1")
                .password("xxx")
                .address("xxx")
                .email("xxxxxx")
                .phone("1111111").build();
        iNextUserTService.userEnroll(enrollUserVO);

    }

    @Test
    void checkUserName() {
    }

    @Test
    void userAuth() {
    }

    @Test
    void describeUserInfo() throws CommonServiceExcetion {
        String s = "2222";
        System.out.println(iNextUserTService.describeUserInfo(s));
    }

    @Test
    void updateUserInfo() throws CommonServiceExcetion {
        UserInfoVO userInfo = UserInfoVO.builder().uuid(6).nickname("王维").build();
        System.out.println(iNextUserTService.updateUserInfo(userInfo));
    }


}